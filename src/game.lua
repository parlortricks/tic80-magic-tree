function clear_table(t)
 for i=0, #t do t[i]=nil end
end

local leafs={}
local n=0
function branch(x,y,size,angle,length,pos,col)
 local n = n + 0.01
 local ov
 local scale=5
 local diam = perlin.lerp(size, 0.7*size, pos/length) --reduce diameter
 diam= mapv(perlin:noise(n),0,1,0.4,1.6) *scale--add noise
 
 elli(x,y,diam,diam,col) --draw branches
	if size > 0.6 then
	 if pos < length then
		 x = x+cos(angle+nrand(-pi/10, pi/10))
		 y = y+sin(angle+nrand(-pi/10, pi/10))
   ov = Vector(x,y)
			branch(x,y,size,angle,length,pos+1,col)
		else
			table.insert(leafs,Vector(x,y))

   local draw_left_branch = function() local v = rnd(1) if v > 0.2 then return true end end
   local draw_right_branch = function() local v = rnd(1) if v > 0.2 then return true end end
   if draw_left_branch then
    branch(x,y,nrand(0.5,0.7)*size,angle-nrand(pi/15,pi/5),nrand(0.6,0.8)*length,0,col)
   end
   
   if draw_right_branch then
    branch(x,y,nrand(0.5,0.7)*size,angle+nrand(pi/15,pi/5),nrand(0.6,0.8)*length,0,col)
   end

   --if none of the branchs are drawn, draw a tip
   if not draw_left_branch and not draw_right_branch then 
     local v1,v2,v3,v4,ov
     local scale=2
     ov=Vector(x,y)
     v1=Vector(0,-diam) + ov
     v2=Vector(2*diam,-diam/3) + ov
     v3=Vector(2*diam,diam/3) + ov
     v4=Vector(0,diam) + ov
     quad(v1.x,v1.y,
          v2.x,v2.y,
          v3.x,v3.y,
          v4.x,v4.y,col)
   end
		end
	end
end

function draw_leafs(min_diam, max_diam)
 local rcol=floor(nrand(0,2))
 local col={5,6,7}
 if rcol ==0 then col = {5,6,7} end
 if rcol ==1 then col = {4,3,2} end

for i=1,#leafs do
 local diam = nrand(min_diam, max_diam)
 local jitterx = nrand(-6,6)
 local jittery = nrand(-6,6)
 local x=leafs[i].x + jitterx
 local y=leafs[i].y + jittery
 --local col = mapv(rnd(0,3),0,3,5,7)
 elli(x, y, diam, diam,col[i%3+1])
end
end

function ground()

end

function create_tree()
local rcol=floor(nrand(0,2))
local col={13,14,15}
if rcol ==0 then col = {15,14,13} end
if rcol ==1 then col = {15,1,2} end
-- if rcol ==2 then col = {15,8,9} end 

for i=0,2 do
 branch(screen_width/2,screen_height,40,-pi/2,nrand(10,40),0,col[i+1])
end
end

function init()
fps = FPS:new()
end

function input()
if keyp(48) then
 pause = not pause
end
if keyp(49) then
 debug = not debug
end
end

local wait=0
init()
function TIC()
input()
cls(11)
 

if not pause then
 
 if wait%60==0 then
  
  --draw_leafs(4,70)
  create_tree()
  draw_leafs(1,5)
  clear_table(leafs)
  memcpy(0x04000,0x0000,16384)
 end

 


end
memcpy(0x0000,0x04000,16384)
wait=wait+1

footer_draw()
stats()
end